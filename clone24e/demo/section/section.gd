extends Node2D

# Variables para spawn_section
var theme = "rocks"
var size = Vector2(200,150)
var from = 70
var to = 90
var broad = 10
var points_number = 20
var points = []
var road
var road_normal

# Called when the node enters the scene tree for the first time.
func _ready():
	spawn_section()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func spawn_section():
	road = Vector2(size.x, to) - Vector2(0, from) 
	road_normal = road.normalized().orthogonal()
	var line_dist = road_normal.dot(Vector2(0,from))
	var point_dist = 0
	# Calcular los puntos de las posiciones
	for i in range(0,points_number):
		var point = Vector2.ZERO
		point.x = randi_range(1, size.x - 1)
		point.y = randi_range(1, size.y - 1)
		point_dist = road_normal.dot(point)
		# Rechazar si está en el tramo reservado
		if abs(point_dist - line_dist) > broad:
			points.append(point)
	# Colocar objetos en sus posiciones

func _draw():
	var rojo = Color(255,0,0)
	var verde = Color(0,255,0)
	var azul = Color(0,0,255)
	var blanco = Color(255,255,255)
	var desp = Vector2(10,10)

	draw_line(desp, desp+Vector2(size.x, 0), azul,1)
	draw_line(desp, desp+Vector2(0, size.y), azul,1)
	draw_line(desp+size, desp+Vector2(size.x, 0), azul,1)
	draw_line(desp+size, desp+Vector2(0, size.y), azul,1)
	
	draw_circle(desp + Vector2(0,from), 2, blanco)
	draw_circle(desp + Vector2(size.x,to), 2, blanco)
	draw_line(desp + Vector2(0, from-broad), desp+Vector2(size.x, to-broad), verde, 1)
	draw_line(desp + Vector2(0, from+broad), desp+Vector2(size.x, to+broad), verde, 1)
	draw_line(desp + Vector2(0, from), desp+Vector2(0, from)+road_normal*10, blanco, 1)

	for point in points:
		draw_circle(desp + point, 2, rojo)
