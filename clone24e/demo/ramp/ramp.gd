extends Node # ramp state

var parent
var speed = 80

func _ready():
	parent = get_parent()

func update(delta):
	parent.get_node("sprite").z_index = 3
	var dir = Vector2.ZERO
	dir.x = Input.get_axis("ui_left", "ui_right")
	dir.y = -dir.x
	dir = dir.normalized()
	parent.position += dir * speed * delta

