extends Area2D

@export var speed = 100

func _ready():
	$anim.play("idle")

func _process(delta):
	var dir = Vector2.ZERO
	dir = Input.get_vector("player_left", "player_right", "player_up", "player_down")	
	position += dir * speed * delta
	z_index = position.y
