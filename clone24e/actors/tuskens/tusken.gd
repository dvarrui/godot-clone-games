extends Area2D

func _ready():
	$sprite.z_index = self.position.y
	if $anim:
		$anim.play("default")
		$anim.z_index = self.position.y

func _process(delta):
	position.x -= delta * Global.speed
