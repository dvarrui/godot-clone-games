
# godot-episodio22

* Descargar los recursos desde: https://drive.google.com/drive/folders/1BjJvtjr9vTQ79SIoPuDS_AQGYvbjiYdF
* Proyectos > Ajustes > Display > Windows
    * width 224
    * height 256
    * mode: Windowed
    * Stretch
        * mode: viewport (2d en Godot3.5)
        * Aspect: keep
        * Scale: 1
    * Rendering > Textures > Nearest
    * Activate advanced > Window ... override > sizex3
* Creamos una escena main
* Creamos scena player con sprite2d y Texture.filter=Nearest
