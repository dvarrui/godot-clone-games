extends Node

enum State {
	PLAYING,
	GAMEOVER,
}
@onready var player = $player
@onready var panel = $panel
@onready var stage1 = $stage1
@onready var timer = $game_over/timer

var state = State.PLAYING

func _ready():
	player.set_parent(self)
	panel.player = player
	stage1.set_parent(self)

func _process(_delta):
	if state == State.PLAYING:
		panel.refresh()

func refresh_panel():
	panel.refresh()

func game_over():
	if state != State.PLAYING:
		return
	state = State.GAMEOVER
	get_node("panel/music").stop()
	get_node("game_over").show()
	timer.wait_time = 2
	timer.start()
	
func _on_timer_timeout():
	get_tree().change_scene_to_file("res://menu.tscn")
