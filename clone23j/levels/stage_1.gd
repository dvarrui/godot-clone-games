extends Node2D

enum State {
	RUNNING,
	STOPPED,
}

@onready var ties_group = $ties_group
@onready var timer_move_side = $ties_group/timer_move_down
@onready var timer_kamikaze_tie = $timer_kamikaze_tie
@onready var timer_shot = $ties_group/timer_shot
@onready var timer_rock = $timer_rock

var Rock = preload("res://actors/rock/rock.tscn")
var TieV = preload("res://actors/tie/tie_v.tscn")
var parent = null
var screen = null
var state = State.RUNNING

func _ready():
	randomize()
	screen = get_viewport_rect().size
	timer_move_side.start()
	timer_shot.start()
	timer_rock.start()
	spawn_ties_group()

func _process(_delta):
	var ties_area = ties_group.get_ties_area()
	if ties_area["min"].x <= 10:
		ties_group.set_offset(Vector2(200,0))
	if ties_area["max"].x >= 214:
		ties_group.set_offset(Vector2(-200,0))

func stop():
	state = State.STOPPED
	timer_rock.stop()
	timer_kamikaze_tie.stop()

func set_parent(value):
	parent = value
	ties_group.set_parent(value)

func spawn_ties_group():
	ties_group.rows = 4
	ties_group.cols = 6
	ties_group.base.x = 10
	ties_group.base.y = 32
	ties_group.init()

func spawn_kamikaze_tie():
	if state != State.RUNNING:
		return

	var tiev = TieV.instantiate()
	tiev.position.x = randi() % int(screen.x)
	tiev.position.y = 0
	var ajust = 64
	if not is_instance_valid(parent.player):
		return

	if tiev.position.x > parent.player.position.x:
			ajust *= (-1)

	var target = Vector2(
		parent.player.position.x + ajust, 
		screen.y
		)
	tiev.set_target(target)
	tiev.speed = 90
	tiev.parent = self
	self.add_child(tiev)

func spawn_rock():
	if state != State.RUNNING:
		return
	var rock = Rock.instantiate()
	rock.position.y = 0
	if (randi() % 2 == 1):
		rock.position.x = (randi() % int(screen.x/2))
	else:
		rock.position.x = screen.x/2 + (randi() % int(screen.x/2))
	self.add_child(rock) 

func _on_timer_rock_timeout():
	spawn_rock()

func _on_timer_shot_timeout():
	if len(ties_group.ties) > 0:
		ties_group.random_shot()
	else:
		parent.game_over()

func _on_timer_move_down_timeout():
	var offset = ties_group.offset
	offset.y = 32
	ties_group.set_offset(offset)

func _on_timer_kamikaze_tie_timeout():
	self.spawn_kamikaze_tie()
