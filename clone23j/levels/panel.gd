extends Node2D

@onready var vbox_score = $hbox/vbox_score/value
@onready var vbox_high = $hbox/vbox_high/value
@onready var vbox_lives = $hbox/vbox_lives/value
var player = null

func _ready():
	refresh()

func refresh():
	var score = 0
	var lives = 0

	if player != null:
		score = player.score
		if Global.high < player.score:
			Global.high = player.score
		if player.lives >= 0:
			lives = player.lives

	vbox_score.text = str(score)
	vbox_high.text = str(Global.high)
	vbox_lives.text = str(lives)
