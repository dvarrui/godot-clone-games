extends Area2D

enum State {
  ALIVE,
  DESTROYED,
}

@onready var sound = $sound
var player = null
var speed = 200
var state = State.ALIVE

func _ready():
	# if (randi() % 10) > 4:
	sound.play()

func _process(delta):
	if state == State.ALIVE:
		position.y -= speed * delta

func remove():
	if !is_queued_for_deletion():
		queue_free()

func _on_area_entered(area):
	if state != State.ALIVE:
		return 
	if area.is_in_group("enemies"):
		player.score += 10
		player.parent.refresh_panel()
	if area.is_in_group("enemies") or area.is_in_group("rocks"):
		state = State.DESTROYED
		self.remove()
		if is_instance_valid(area) or !area.is_queued_for_deletion():
			area.hit()
