extends Area2D

enum State {
  ALIVE,
  DESTROYED,
  TOREMOVE,
}

var Laser = preload("res://actors/player/laser.tscn")

@onready var laser_point = $laser_point
@onready var sprite = $sprite
@onready var sound = $sound
@onready var timer = $timer
@export var speed = 150

var can_shot = true
var dir = Vector2.ZERO
var lives = 2
var parent = null
var score = 0
var state = State.ALIVE
var screen = Vector2.ZERO
signal player_killed

func _ready():
	screen = get_viewport_rect().size
	sprite.set_frame(0)
	# self.connect("player_killed", get_parent().player_killed())

func _process(delta):
	if state == State.DESTROYED:
		return
	dir = Vector2.ZERO
	if Input.is_action_pressed("player_right") and position.x < screen.x - 8:
		dir.x += 1
	if Input.is_action_pressed("player_left") and position.x > 8:
		dir.x -= 1
	if Input.is_action_pressed("player_up") and position.y > 8 :
		dir.y -= 1
	if Input.is_action_pressed("player_down") and position.y < screen.y - 8:
		dir.y += 1
	if Input.is_action_just_pressed("player_shot") and can_shot == true:
		shot()

	self.position += dir.normalized() * speed * delta
	
func hit():
	if state != State.ALIVE:
		return
	state = State.DESTROYED
	sound.play()
	timer.stop()
	timer.wait_time = 0.7
	timer.start()
	sprite.set_frame(1)
	lives -= 1

func remove():
	if !self.is_queued_for_deletion():
		get_parent().remove_child(self)
		queue_free()

func set_parent(value):
	parent = value

func shot():
	if state != State.ALIVE:
		return
	var laser = Laser.instantiate()
	laser.player = self
	laser.global_position = laser_point.global_position
	self.get_parent().add_child(laser)
	can_shot = false
	timer.wait_time = 0.2
	timer.start()

func spawn():
	if lives < 0:
		parent.game_over()
		self.remove()
	state = State.ALIVE
	sprite.set_frame(0)
	can_shot = true
	timer.start()
	show()

func _on_timer_timeout():
	if state == State.ALIVE:
		can_shot = true
	if state == State.DESTROYED:
		self.spawn()
	if state == State.TOREMOVE:
		self.remove()

func _on_area_entered(area):
	if area.is_in_group("enemies"):
		self.hit()
		area.hit()
	elif area.is_in_group("rocks"):
		self.hit()
