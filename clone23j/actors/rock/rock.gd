extends Area2D

@onready var flash = $flash
@onready var sprite = $sprite
@onready var timer = $timer

@export var speed = 10

var screen = null

func _ready():
	screen = get_viewport_rect().size

func _process(delta):
	self.position.y += speed * delta
	if self.position.y > screen.y:
		self.remove()

func remove():
	if !is_queued_for_deletion():
		queue_free()
	
func hit():
	sprite.hide()
	timer.wait_time = 0.04
	timer.start()

func _on_timer_timeout():
	sprite.show()
