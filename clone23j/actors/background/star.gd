extends Node2D

@onready var sprite = $sprite
var current = 0
var speed = 0
var screen = null
var types = [
	 { "modulate": 1.00, "speed": 8.0},
	 { "modulate": 0.50, "speed": 5.0},
	 { "modulate": 0.3, "speed": 1.0},
]

func _ready():
	screen = get_viewport_rect().size
	current = randi() % len(types)
	speed = types[current]["speed"]
	modulate_sprite()

func _process(delta):
	self.position.y += speed * delta
	if self.position.y > screen.y:
		reset()

func reset():
	self.position.y = 0
	self.position.x = float(randi() % int(screen.x))
	current = randi() % len(types)
	speed = types[current]["speed"]
	modulate_sprite()

func modulate_sprite():
	var value = types[current]["modulate"]
	var color = Color(value, value, value)
	sprite.set_self_modulate(color)
