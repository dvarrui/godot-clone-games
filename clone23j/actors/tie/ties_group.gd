extends Node2D

@export var rows = 2
@export var cols = 3
@export var base = Vector2.ZERO
var offset = Vector2.ZERO
var parent = null
var Tie = preload("res://actors/tie/tie.tscn")
var ties = []

func init():
	ties = []
	for row in rows:
		for col in cols:
			var tie = Tie.instantiate()
			tie.position.x = base.x + col * 22
			tie.position.y = base.y + row * 20
			tie.speed = 20
			tie.id = len(ties)
			tie.parent = self
			tie.set_target(tie.position + offset)
			ties.append(tie)
			self.add_child(tie)

func get_ties_area():
	var area = { 
		"min": Vector2.ZERO, 
		"max": Vector2.ZERO,
	}
	if len(ties) == 0:
		return area

	area = { 
		"min": ties[0].position, 
		"max": ties[0].position,
	}
	for tie in ties:
		if tie.position.x < area["min"].x:
			area["min"].x = tie.position.x
		if tie.position.x > area["max"].x:
			area["max"].x = tie.position.x
		if tie.position.y < area["min"].y:
			area["min"].y = tie.position.y
		if tie.position.y > area["max"].y:
			area["max"].y = tie.position.y
	return area
	
func random_shot():
	if len(ties) > 0:
		var index = (randi() % len(ties))
		ties[index].shot()

func remove_tie(id):
	var aux = []
	for tie in ties:
		if tie.id != id:
			aux.append(tie)
	ties = aux

func set_parent(value):
	parent = value

func set_offset(value):
	offset = value
	for tie in ties:
		if tie != null:
			var t = tie.get_target()
			tie.set_target(tie.position + value)
