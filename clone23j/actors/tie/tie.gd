extends Area2D

enum State {
	ALIVE,
	DESTROYED
}

@onready var laser_point = $laser_point
@onready var sound = $sound
@onready var sprite = $sprite
@onready var timer_remove = $timer_remove
@onready var timer_shot = $timer_shot

@export var speed = 20

var Laser = preload("res://actors/tie/laser.tscn")
var dir = Vector2.ZERO
var id = 0
var parent = null
var screen = Vector2.ZERO
var state = State.ALIVE
var target = Vector2(-1, -1)

func _ready():
	screen = get_viewport_rect().size
	if target == Vector2(-1, -1):
		target = self.position
		target.y = screen.y
	dir = (target - position).normalized()
	timer_shot.wait_time = 0.4
	timer_shot.start()

func _process(delta):
	move(delta)

func hit():
	if state == State.DESTROYED:
		return
	state = State.DESTROYED
	if self.id >= 0:
		parent.remove_tie(self.id)
	sprite.set_frame(1)
	sound.play()
	speed -= speed / 2.5
	timer_remove.wait_time = 0.3
	timer_remove.start()

func move(delta):
	update_dir()
	self.position += dir * speed * delta
	if self.position.y > screen.y:
		remove()

func remove():
	if !self.is_queued_for_deletion():
		get_parent().remove_child(self)
		queue_free()

func set_target(value):
	target = value
	update_dir()

func get_target():
	return target

func shot():
	if state == State.ALIVE:
		var laser = Laser.instantiate()
		laser.global_position = laser_point.global_position
		self.get_parent().add_child(laser)

func update_dir():
	dir = (target - position).normalized()
	
func _on_timer_remove_timeout():
	remove()

func _on_timer_shot_timeout():
	if self.id == -1:
		self.shot()

