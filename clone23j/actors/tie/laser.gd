extends Area2D

enum State {
  ALIVE,
  DESTROYED,
}

@onready var sound = $sound
var speed = 150
var state = State.ALIVE

func _ready():
	sound.play()

func _process(delta):
	if state == State.ALIVE:
		position.y += speed * delta

func _on_area_entered(area):
	if state != State.ALIVE:
		return 
	if area.is_in_group("player"):
		state = State.DESTROYED
		if is_instance_valid(area) or !area.is_queued_for_deletion():
			area.hit()
		if !is_queued_for_deletion():
			queue_free()
