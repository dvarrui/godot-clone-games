extends Node

var parent
var speed = 50

func _ready():
	parent = get_parent()

func update(delta):
	if parent.ramp_in_y < parent.position.y:
		parent.set_state("land")
		return
	parent.get_node("sprite").z_index = 3
	var dir = Vector2.ZERO
	dir.x = 100
	dir.y = +150
	parent.position += dir * delta

