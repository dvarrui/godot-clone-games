extends Area2D

var state = "land"
var current 
var ramp_in_y = 0

func set_state(value):
	print("STATE:", value)
	state = value
	current = get_node(state)

func _ready():
	set_state("land")

func _process(delta):
	current.update(delta)

func _on_area_entered(area):
	if area.is_in_group("ramp"):
		ramp_in_y = position.y
		set_state("ramp")

func _on_area_exited(area):
	if area.is_in_group("ramp") and state=="ramp":
		set_state("air")
