extends Node

var parent
var speed = 100

func _ready():
	parent = get_parent()

func update(delta):
	var dir = Vector2.ZERO
	dir.x = Input.get_axis("ui_left", "ui_right")
	dir.y = Input.get_axis("ui_up", "ui_down")
	dir = dir.normalized()
	parent.position += dir * speed * delta

