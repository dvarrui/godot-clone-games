
# Clone Games

Este proyecto es un conjunto de minijuegos con las siguientes características:
* Se usa el motor de videojuegos **GodotEngine** 4.1.X y GDScript como lenguaje de scripting.
* Los gráficos tienen estilo **pixelart**, usando celdas de 16x16 principalmente y escala de grises (6 niveles).
* Las mecánicas de cada minijuego imitan a un **juego clásico** 2D,pero ambientado en el mundo de **StarWars**.

## Clone23j - Space Invaders

![](images/clone23j.png)

* El jugador (XWing) debe ir destruyendo la flota de Tie Fighters antes de que lo destruyan.
* Inspirado en [Curso Godot | Space Invaders](https://www.youtube.com/watch?v=uL_2fr2F2eg&list=PL1FkQ8zpzVaEPS7EKZGlTPmhkR4WQC8UD&index=2)

## Clone23s - Collect pieces

![](images/clone23s.png)

* El jugador(Mando) debe ir cogiendo todas las cajas antes de que acabe el tiempo, evitando caer en las garras del Sharlack.
* Inspirado en el proyecto [Coin Dash](hhttps://github.com/PacktPublishing/Godot-4-Game-Development-Projects-Second-Edition/tree/main/Chapter02%20-%20Coin%20Dash/coin_dash)

## Clone23n - Asteroids

![](images/clone23n.png)

* El jugador controla el "Halcón Milenario" para eliminar los asteroides que pueden dañar la nave. A medida que pasa el tiempo las naves del imperio se acercan y nos lo ponen más difícil.
* Inspirado en el proyecto [Space Rocks](https://github.com/PacktPublishing/Godot-4-Game-Development-Projects-Second-Edition/tree/main/Chapter03%20-%20Space%20Rocks/space_rocks)
