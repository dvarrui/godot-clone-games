extends Node2D

var screensize = Vector2(220, 200)
@export var LandScene : PackedScene
@export var lands_number = 8

func _ready():
	spawn_lands()

func spawn_lands():
	for i in range(0, lands_number):
		var land = LandScene.instantiate()
		land.position = Vector2(randi_range(8, screensize.x-8), randi_range(108, screensize.y-8))
		$lands.add_child(land)
