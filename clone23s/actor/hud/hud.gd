extends CanvasLayer

@onready var time = $margin/hbox/time/value
@onready var score = $margin/hbox/score/value
@onready var high = $margin/hbox/high/value

func _ready():
	high.text = str(Global.hscore)
	
func update_score(value):
	score.text = str(value)
	if value > Global.hscore:
		Global.hscore = value
		high.text = str(Global.hscore)

func update_timer(value):
	time.text = str(value)

func show_message(text):
	$message.text = text
	$message.show()
	$timer.start()

func _on_timer_timeout():
	$message.hide()
