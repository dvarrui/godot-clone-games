extends Area2D

signal pickup
signal hurt

@export var speed = 100

var velocity = Vector2.ZERO
var screensize = Vector2.ZERO

func _process(delta):
	velocity = Input.get_vector("player_left", "player_right",
		"player_up", "player_down")
	position += velocity * speed * delta
	position.x = clamp(position.x, 4, screensize.x-4)
	position.y = clamp(position.y, 100-8, screensize.y-8)

	if velocity.length() > 0:
		$anim.animation = "run"
	else:
		$anim.animation = "idle"
	if velocity.x != 0:
		$anim.flip_h = velocity.x < 0

func start():
	set_process(true)
	position = screensize / 2
	$anim.animation = "idle"

func die():
	set_process(false)
	$anim.animation = "die"

func _on_area_entered(area):
	if area.is_in_group("items"):
		area.pickup()
		pickup.emit("item")
	if area.is_in_group("powerups"):
		area.pickup()
		pickup.emit("powerup")
	if area.is_in_group("enemies"):
		hurt.emit()
		die()
