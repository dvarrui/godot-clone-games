extends Area2D

var screensize = Vector2.ZERO

func _ready():
	$sprite.frame = randi_range(0,2)
	$sprite.flip_h = (randi_range(0,1) == 1)
	$timer.start(randf_range(0, 4))
	$anim.hide()
	init_position()

func init_position():
	position = Vector2(randi_range(8, screensize.x-8), randi_range(108, screensize.y-8))


func pickup():
	$shape.set_deferred("disabled", true)
	var tw = create_tween().set_parallel().set_trans(Tween.TRANS_QUAD)
	tw.tween_property(self, "scale", scale * 1.5, 0.1)
	tw.tween_property(self, "modulate:a", 0.0, 0.1)
	await tw.finished
	queue_free()

func _on_timer_timeout():
	$anim.show()
	$anim.frame = 0
	$anim.play()


func _on_area_entered(area):
	if area.is_in_group("enemies"):
		init_position()
