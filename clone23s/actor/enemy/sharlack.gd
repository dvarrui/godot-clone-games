extends Area2D

func _ready():
	$anim.show()
	$anim.stop()


func _on_timer_timeout():
	if $anim.is_playing():
		$anim.hide()
		$anim.stop()
		$timer.start()
	else:
		$anim.show()
		$anim.play("tentacles")
		$timer.start()
