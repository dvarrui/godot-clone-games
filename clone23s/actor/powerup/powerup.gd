extends Area2D

func _ready():
	$anim.play()

func pickup():
	$shape.set_deferred("disabled", true)
	var tw = create_tween().set_parallel().set_trans(Tween.TRANS_QUAD)
	tw.tween_property(self, "scale", scale * 2, 0.2)
	tw.tween_property(self, "modulate:a", 0.0, 0.2)
	await tw.finished
	queue_free()

func _on_lifetime_timeout():
	queue_free()


