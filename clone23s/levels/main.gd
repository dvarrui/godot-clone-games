extends Node2D

@export var ItemScene : PackedScene
@export var PowerupScene : PackedScene
@export var playtime = 20

var playing = true
var level = 1
var score = 0
var time_left = 0
var screensize = Vector2(220, 200)

func _ready():
	screensize = get_viewport().get_visible_rect().size
	$player.screensize = screensize
	new_game()

func new_game():
	level = 1
	score = 0
	time_left = playtime
	$player.start()
	$timer/game.start()
	spawn_items()
	$hud.show_message("COLLECT\nPIECES!")

func spawn_items():
	$sounds/level.play()
	for i in level + 4:
		var item = ItemScene.instantiate()
		item.screensize = screensize
		$temp.add_child(item)
	if time_left < 15:
		# spawn powerup
		var item = PowerupScene.instantiate()
		$temp.add_child(item)
		item.position = Vector2(randi_range(8, screensize.x-8), randi_range(108, screensize.y-8))

func _process(_delta):
	if playing and get_tree().get_nodes_in_group("items").size() == 0:
		level += 1
		time_left += 5
		spawn_items()

func _on_player_pickup(type):
	match type:
		"item":
			$sounds/pickup.play()
			score += 1
			$hud.update_score(score)
		"powerup":
			$sounds/powerup.play()
			time_left += 5
			$hud.update_timer(time_left)

func _on_game_timeout():
	time_left -= 1
	$hud.update_timer(time_left)
	if time_left <= 0:
		game_over()

func _on_player_hurt():
	game_over()

func game_over():
	playing = false
	$timer/game.stop()
	get_tree().call_group("items", "queue_free")
	$hud.show_message("GAME OVER!")
	$sounds/gameover.play()
	$player.die()
	await get_tree().create_timer(4.0).timeout
	get_tree().change_scene_to_file("res://menu.tscn")


func _on_powerup_timeout():
	var p = PowerupScene.instantiate()
	$temp.add_child(p)
	p.screensize = screensize
	p.position = Vector2(randi_range(0, screensize.x), randi_range(0, screensize.y))
