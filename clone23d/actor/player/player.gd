extends CharacterBody2D

var speed = 100.0
var acceleration = 1000.0
var direction = Vector2.ZERO
var state = ["idle", "d"]
@onready var anim = $anim

func _ready():
	update_anim()

func read_input():
	direction = Vector2.ZERO
	direction.x = Input.get_axis("player_left", "player_right")
	direction.y = Input.get_axis("player_up", "player_down")
	direction = direction.normalized()

func update_anim(vel = Vector2.ZERO):
	if vel == Vector2.ZERO:
		state[0] = "idle"
	else:
		state[0] = "walk"
		if vel.x == 0:
			if vel.y > 0:
				state[1] = "d"
			else:
				state[1] = "u"
		else:
			if vel.x > 0:
				state[1] = "r"
				anim.flip_h = false
			else:
				state[1] = "r"
				anim.flip_h = true
	anim.play(state[0]+"_"+state[1])

func _physics_process(delta):
	read_input()
	if direction != Vector2.ZERO:
		velocity = direction * speed
	else:
		velocity = Vector2.ZERO
	update_anim(velocity)
	move_and_slide()

