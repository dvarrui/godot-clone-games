extends Node2D


# Called when the node enters the scene tree for the first time.
func _ready():
	var tw = create_tween().set_parallel().set_trans(Tween.TRANS_QUAD)
	tw.tween_property($sprite, "modulate:a", 0.0, 2)
	tw.tween_property($sprite, "scale:x", 2, 2)
	tw.tween_property($sprite, "scale:y", 2, 2)
	await tw.finished


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
