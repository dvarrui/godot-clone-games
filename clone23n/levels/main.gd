extends Node2D

@export var EnemyScene : PackedScene
@export var DestroyerScene: PackedScene
@export var BigExplosionScene : PackedScene
@export var PowerupScene : PackedScene
@export var RockScene : PackedScene

enum {PLAYING, INIT, GAMEOVER}
var screensize = Vector2.ZERO
var state = PLAYING
var level = 0
var score = 0

func _ready():
	screensize = get_viewport().get_visible_rect().size

func _process(_delta):
	var rocks = get_tree().get_nodes_in_group("rocks").size()
	if state == PLAYING and rocks == 0:
		state = INIT
		create_new_level()

func create_new_level():
	await get_tree().create_timer(0.5).timeout
	level += 1 
	$hud.show_message("LEVEL " + str(level))
	$audio/levelup.play()

	await get_tree().create_timer(1).timeout
	spawn_powerup()
	var terrain = level
	while terrain > 0:
		var _size = randi_range(1, 4)
		if terrain <= 4:
			_size = randi_range(1, terrain)
		terrain -= _size
		spawn_rock(_size)
	$timer/enemy.start(randf_range(5, 10))
	state = PLAYING
	
func spawn_rock(size=4, pos=null, vel=null, ):
	if pos == null:
		$rock_path/rock_spawn.progress = randi()
		pos = $rock_path/rock_spawn.position
	if vel == null:
		vel = Vector2.RIGHT.rotated(randf_range(0, TAU)) * randf_range(50, 100)
	var rock = RockScene.instantiate()
	rock.screensize = screensize
	rock.init(size, pos, vel)
	rock.exploded.connect(self._on_rock_exploded)
	$temp.call_deferred("add_child", rock)
	
func spawn_powerup():
	var powerup
	if $player.power >= 100:
		powerup = PowerupScene.instantiate()
		powerup.init("points", 100)
		powerup.connect("pickup", self._on_powerup_pickup)
	else:
		powerup = PowerupScene.instantiate()
		powerup.connect("pickup", self._on_powerup_pickup)
	$temp.add_child(powerup)

func _on_powerup_pickup(type, value):
	match type:
		"points":
			score += value
			$hud.update_score(score)
		"power":
			$player.add_power(value)
			$hud.update_power($player.power)

func _on_rock_exploded(size, pos, vel):
	score += size * 10
	$hud.update_score(score)
	if size > 1:
		for offset in [-1, 1]:
			var dir = $player.position.direction_to(pos).orthogonal() * offset
			var newpos = pos + dir * size * 2
			var newvel = dir * vel.length() * 1.1
			spawn_rock(size - 1, newpos, newvel)

func _on_player_dead(_position):
	state = GAMEOVER
	$hud.show_message("GAME OVER")
	var bigexplosion = BigExplosionScene.instantiate()
	bigexplosion.position = _position
	add_child(bigexplosion)
	$timer/game_over.start()

func _on_game_over_timeout():
	get_tree().change_scene_to_file("res://menu.tscn")

func _on_enemy_timeout():
	if level < 4:
		return
	# Spawn new enemy on main scene
	var enemy = EnemyScene.instantiate()
	if level < 6:
		enemy.get_node("gun_cooldown").autostart = false
	if level > 8:
		var destroyer = DestroyerScene.instantiate()
		destroyer.target = $player
		$temp.add_child(destroyer)

	$temp.add_child(enemy)
	enemy.target = $player
	$timer/enemy.start(randf_range(20, 40))
