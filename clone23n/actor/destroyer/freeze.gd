extends Area2D

var speed = 70
var dir = 1

func _ready():
	$anim.play("default")
	if dir == 1:
		$sprite.flip_h = true
	else:
		$sprite.flip_h = false

func _process(delta):
	position.x += dir * speed * delta

func _on_visibility_screen_exited():
	queue_free()

func _on_body_entered(body):
	if body.is_in_group("player"):
		$shape.disabled = true
		body.start_freeze()
		queue_free()
