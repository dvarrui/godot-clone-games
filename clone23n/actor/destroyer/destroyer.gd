extends Area2D

enum {ALIVE, SHOOT}
@export var FreezeScene: PackedScene
@export var speed = 20
var screen
var target
var target_offset = 4
var state = ALIVE

func _ready():
	randomize()
	screen = get_viewport_rect().size
	position.x = float(randi() % int(screen.x))
	$audio.play()

func _process(delta):
	position.y += speed * delta
	if $audio.finished:
		$audio.play()
	if state == ALIVE and abs(target.position.y - position.y) < target_offset:
		state = SHOOT
		var freeze: Area2D = FreezeScene.instantiate()
		freeze.global_position = self.global_position
		if target.position.x < position.x:
			freeze.dir = -1
		get_tree().root.add_child(freeze)

func hit():
	pass

func _on_body_entered(body):
	if body.is_in_group("player"):
		body.damage(100)

func _on_visibility_screen_exited():
	queue_free()

