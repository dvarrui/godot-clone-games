extends Node2D

func _ready():
	$particles.emitting = true
	$audio.play()

func _on_timer_timeout():
	queue_free()
