extends Node2D

var dir = Vector2(1, 1).normalized()
var lifetime = 1.3
var speed = 50

func _ready():
	$anim.play("default")
	var tw = create_tween().set_parallel().set_trans(Tween.TRANS_QUAD)
	tw.tween_property(self, "scale", scale * 0.95, lifetime)
	tw.tween_property(self, "modulate:a", 0.0, lifetime)
	await tw.finished
	queue_free()

func start(_pos, _dir, _lifetime=1.3):
	dir = _dir.normalized()
	position = _pos
	lifetime = _lifetime

func _process(delta):
	position += dir * speed * delta
