extends RigidBody2D

signal exploded

@export var ExplosionScene : PackedScene
@export var MynockScene: PackedScene
var screensize = Vector2.ZERO
var size :int = 4
var life :int = 1
var sprite 
var shape 

func _ready():
	$sprite1.frame = randi_range(0, 2)
	$sprite2.frame = randi_range(0, 3)

func init(_size, _position, _velocity):
	size = _size
	position = _position
	linear_velocity = _velocity
	angular_velocity = randf_range(-PI, PI)
	if size > 2:
		life = 2
	for i in range(1,5):
		if i == size:
			sprite = get_node("sprite" + str(i))
			shape = get_node("shape" + str(i))
			sprite.show()
			shape.disabled = false
		else:
			get_node("sprite" + str(i)).hide()
			get_node("shape" + str(i)).disabled = true

func _integrate_forces(physics_state):
	var xform = physics_state.transform
	var margin = size # * 5
	xform.origin.x = wrapf(xform.origin.x, 0-margin, screensize.x+margin)
	xform.origin.y = wrapf(xform.origin.y, 0-margin, screensize.y+margin)
	physics_state.transform = xform

func hit():
	life -= 1
	if life <= 0:
		explode()
	else:
		sprite.self_modulate = Color(0.5,0.5,0.5,0.5)
		$timer.wait_time = 0.06
		$timer.start()

func _on_timer_timeout():
	sprite.self_modulate = Color(1,1,1,1)

func explode():
	if size == 4:
		spawn_mynock()
	shape.set_deferred("disabled", true)
	sprite.hide()

	var explosion = ExplosionScene.instantiate()
	explosion.position = position
	get_tree().root.add_child(explosion)

	exploded.emit(size, position, linear_velocity)
	linear_velocity = Vector2.ZERO
	angular_velocity = 0
	queue_free()

func spawn_mynock():
	var pos = self.global_position 
	var dir = linear_velocity.rotated(0.3)
	
	# First mynock
	var mynock = MynockScene.instantiate()
	mynock.start(pos, dir)
	get_tree().root.add_child(mynock)

	# Second mynock
	mynock = MynockScene.instantiate()
	dir = dir.rotated(-0.3)
	pos += Vector2(randf_range(-3,3), randf_range(-3,3))
	mynock.start(pos, dir)
	mynock.speed -= 10
	get_tree().root.add_child(mynock)

