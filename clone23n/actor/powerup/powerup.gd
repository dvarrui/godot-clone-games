extends Area2D

signal pickup(type, value)

enum {ALIVE, WARNING, NONE}
@export var lifetime = 25
var screensize = Vector2.ZERO
var state = ALIVE
var sprite
var type = "power"
const TYPES = { "crystal": 0, "power": 1, "points": 2}
var value = 100

func _ready():
	screensize = get_viewport_rect().size
	var margin = 32
	var x = randi_range(margin, screensize.x - margin)
	var y = randi_range(margin * 2 , screensize.y - margin)
	init(type, value)
	position = Vector2(x, y)
	$lifetime.wait_time = lifetime - 5
	$lifetime.start()

func init(_type, _value):
	type = _type
	value = _value
	$sprite.frame = TYPES[type]
	$sprite.show()
	

func _on_body_entered(body):
	if state == NONE:
		return
	if body.is_in_group("player"):
		state = NONE
		$lifetime.stop()
		$shape.disabled = true
		$audio.play()
		pickup.emit(type, value)
		var tw = create_tween().set_parallel().set_trans(Tween.TRANS_QUAD)
		tw.tween_property($sprite, "scale", self.scale * 1.5, 0.1)
		tw.tween_property($sprite, "modulate:a", 0.0, 0.1)
		await $audio.finished
		queue_free()

func _on_lifetime_timeout():
	if state == ALIVE:
		state = WARNING
		$lifetime.wait_time = 5
		$lifetime.start()
		$anim.play("default")
	elif state == WARNING:
		queue_free()
