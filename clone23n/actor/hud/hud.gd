extends CanvasLayer

@onready var power = $margin/hbox/power/pbar
@onready var score = $margin/hbox/score/value
@onready var high = $margin/hbox/high/value
@onready var message = $message

func _ready():
	high.text = str(Global.hscore)

func show_message(text):
	message.text = text
	message.show()
	$timer.start()

func update_score(value):
	score.text = str(value)
	if value > Global.hscore:
		Global.hscore = value
		high.text = str(value)

func update_power(value):
	power.value = value
	if value == 100:
		$anim.play("power")

func _on_timer_timeout():
	message.hide()
	message.text = ""

func _on_player_power_changed(value):
	update_power(value)
