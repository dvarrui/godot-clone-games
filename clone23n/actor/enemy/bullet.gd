extends Area2D

@export var speed = 130

func start(_pos, _dir):
	position = _pos
	rotation = _dir.angle()
	$sprite.play("default")

func _process(delta):
	position += transform.x * speed * delta

func _on_body_entered(body):
	if body.is_in_group("player"):
		body.damage(30)
		queue_free()

func _on_visibility_screen_exited():
	queue_free()
