extends Area2D

signal explode

@export var BulletScene : PackedScene
@export var ExplosionScene: PackedScene
@export var speed = 120
@export var rotation_speed = 120
@export var health = 3
@export var bullet_spread = 0.2
var follow = PathFollow2D.new()
var target = null

func _ready():
	randomize()
	var path = $paths.get_children()[randi() % $paths.get_child_count()]
	path.add_child(follow)
	follow.loop = false

func _process(delta):
	if not $audio.playing:
		$audio.play()

func _physics_process(delta):
	follow.progress += speed * delta
	position = follow.global_position
	if follow.progress_ratio >= 1:
		queue_free()

func _on_body_entered(body):
	if body.is_in_group("player"):
		body.damage(50)
		hit()

func _on_gun_cooldown_timeout():
	$gun_cooldown.wait_time = 3
	shoot()
	# shoot_pulse(3, 1)

func hit():
	explode.emit()
	$audio.stop()
	var explosion = ExplosionScene.instantiate()
	explosion.position = position
	get_parent().add_child(explosion)
	queue_free()

func shoot():
	if target == null:
		return
	var dir = global_position.direction_to(target.global_position)
	dir = dir.rotated(randf_range(-bullet_spread, bullet_spread))
	var bullet = BulletScene.instantiate()
	get_tree().root.add_child(bullet)
	bullet.start(global_position, dir)

func shoot_pulse(n, delay):
	for i in n:
		shoot()
		await get_tree().create_timer(delay).timeout
