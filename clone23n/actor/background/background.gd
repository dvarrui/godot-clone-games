extends Node2D

@export var num_stars = 30
@export var StarScene: PackedScene 
var screen = null

func _ready():
	randomize()	
	screen = get_viewport_rect().size
	for _index in range(0, num_stars):
		var star = StarScene.instantiate()
		star.position.y = (randi() % int(screen.y))
		star.position.x = (randi() % int(screen.x))
		$temp.add_child(star)
