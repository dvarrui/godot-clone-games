extends Node2D

@onready var sprite = $sprite
var current = 0
var speed = 0
var speed_factor = 4
var screen = null
var types = [
	 { "modulate": 1.00, "speed": 8.0},
	 { "modulate": 0.50, "speed": 5.0},
	 { "modulate": 0.3, "speed": 1.0},
]

func _ready():
	screen = get_viewport_rect().size
	current = randi() % len(types)
	speed = types[current]["speed"] * speed_factor
	modulate_sprite()

func _process(delta):
	self.position.y += speed * delta
	if self.position.y > screen.y:
		reset()

func reset():
	randomize()
	self.position.y = 0
	self.position.x = float(randi() % int(screen.x))
	current = randi() % len(types)
	speed = types[current]["speed"]
	modulate_sprite()
	select_frame()

func modulate_sprite():
	var value = types[current]["modulate"]
	var color = Color(value, value, value)
	sprite.set_self_modulate(color)

func select_frame():
	var s = randi_range(0,100)
	$sprite.frame = 0
	if s > 85:
		$sprite.frame = 1
	if s > 98:
		$sprite.frame = 2
