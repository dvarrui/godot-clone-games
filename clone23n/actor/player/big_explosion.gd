extends Node2D

@export var ExplosionScene : PackedScene

var size = 8
var max = 10
var counter = 0

func init(_size, _max):
	size = _size
	max = _max
	spawn_explosion()

func spawn_explosion():
	counter += 1
	if counter > max:
		queue_free()
	var explosion = ExplosionScene.instantiate()
	var x = randi_range(-size, size)
	var y = randi_range(-size, size)
	explosion.position = Vector2(x, y)
	$temp.add_child(explosion)
	$timer.start()

func _on_timer_timeout():
	spawn_explosion()
