extends RigidBody2D

signal power_changed(power)
signal dead(position)

enum {INIT, ALIVE, FREEZE, DEAD}
var state = INIT

@export var engine_power = 500
@export var spin_power = 8000
@export var fire_rate = 0.25
@export var BulletScene : PackedScene
@export var ExplosionScene : PackedScene

var can_shoot = false
var freeze_position = Vector2.ZERO
var power = 100
var rotation_dir = 0
var screensize = Vector2.ZERO
var thrust = Vector2.ZERO
var tremor_offset = 2

func _ready():
	screensize = get_viewport_rect().size
	$timer/gun_cooldown.wait_time = fire_rate
	change_state(ALIVE)

func change_state(new_state):
	match new_state:
		ALIVE:
			$shape.set_deferred("disabled", false)
			$sprite.modulate.a = 1.0
		FREEZE:
			freeze_position = self.position
			$sprite.self_modulate = Color(0.31, 0.65, 0.78, 1)
			$engine.hide()
			$timer/tremor.start()
		DEAD:
			$shape.set_deferred("disabled", true)
			$sprite.hide()
			$engine.hide()
			linear_velocity = Vector2.ZERO
			dead.emit(position)
	state = new_state

func _process(_delta):
	if state == ALIVE:
		get_input()
	if state == FREEZE:
		tremor()

func get_input():
	if state != ALIVE:
		return
	thrust = Vector2.ZERO
	if state in [DEAD, INIT]:
		return
	if Input.is_action_pressed("thrust"):
		thrust = transform.x * engine_power
		$engine.show()
	else:
		$engine.hide()
	rotation_dir = Input.get_axis("rotate_left", "rotate_right")
	if Input.is_action_pressed("player_shot") and can_shoot:
		shoot()

func _physics_process(_delta):
	constant_force = thrust
	constant_torque = rotation_dir * spin_power

func _integrate_forces(physics_state):
	var xform = physics_state.transform
	xform.origin.x = wrapf(xform.origin.x, 0, screensize.x)
	xform.origin.y = wrapf(xform.origin.y, 0, screensize.y)
	physics_state.transform = xform

func shoot():
	if state != ALIVE:
		return
	can_shoot = false
	$timer/gun_cooldown.start()
	var bullet = BulletScene.instantiate()
	get_parent().get_node("temp").add_child(bullet)
	bullet.start($gun.global_transform)
	bullet.set_process(true)

func _on_gun_cooldown_timeout():
	can_shoot = true

func _on_body_entered(body):
	if body.is_in_group("rocks"):
		body.hit()
		damage(body.size * 10)

func damage(value):
	power -= value
	$sprite.self_modulate = Color(1, 0.0, 0.5, 0.5)
	$timer/damage.wait_time = 0.1
	$timer/damage.start()
	if power <= 0:
		power = 0
		change_state(DEAD)
	power_changed.emit(power)

func _on_damage_timeout():
	$sprite.self_modulate = Color(1, 1, 1, 1)
	$audio.play()

func add_power(value):
	power += value
	if power > 100:
		power = 100
	power_changed.emit(power)

func start_freeze():
	if state != ALIVE:
		return
	print("start_freeze()")
	change_state(FREEZE)

func tremor():
	var x = randf_range(-tremor_offset, tremor_offset)
	var y = randf_range(-tremor_offset, tremor_offset)
	var tremor_position = Vector2(x, y)
	self.position = freeze_position + tremor_position

func _on_tremor_timeout():
	$sprite.self_modulate = Color(1, 1, 1, 1)
	self.position = freeze_position
	change_state(ALIVE)
