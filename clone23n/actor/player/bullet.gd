extends Area2D

@export var speed = 300
var velocity = Vector2.ZERO

func _ready():
	$audio.pitch_scale = 1 + randf_range(-0.2,0.2)
	$audio.play()

func start(_transform):
	transform = _transform
	velocity = transform.x * speed

func _process(delta):
	position += velocity * delta

func _on_visible_screen_exited():
	queue_free()

func _on_body_entered(body):
	if body.is_in_group("rocks"):
		body.hit()
		queue_free()

func _on_area_entered(area):
	if area.is_in_group("enemies"):
		area.hit()
		queue_free()
