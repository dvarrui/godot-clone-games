extends Node2D

enum State {
  PLAY,
  EXIT,
}

@onready var exit = $title/exit
@onready var play = $title/play
@onready var sound = $title/sound
@onready var title = $title
var state = State.PLAY
var enable_color = Color(1, 1, 1)
var disable_color = Color(0.5, 0.5, 0.5)

func _process(_delta):
	if state == State.PLAY:
		play.set_self_modulate(enable_color)
		exit.set_self_modulate(disable_color)
	else:
		play.set_self_modulate(disable_color)
		exit.set_self_modulate(enable_color)
	
	if Input.is_action_just_pressed("player_up"):
		if state != State.PLAY:
			sound.play()
		state = State.PLAY
	if Input.is_action_just_pressed("player_down"):
		if state != State.EXIT:
			sound.play()
		state = State.EXIT
	if Input.is_action_just_pressed("player_shot"):
		self.exec_menu_action()

func exec_menu_action():
	if state == State.PLAY:
		# Init game
		get_tree().change_scene_to_file("res://levels/main.tscn")
	if state == State.EXIT:
		get_tree().quit()
